# top=decoding::decoding_test_harness

from spade import *

async def check_insn(s, insn, f1, v1, f2, v2, f3=None, v3=None):
    s.i.insn = insn
    await cocotb.triggers.Timer(1, units="ps")
    # TODO: Delay
    f1.assert_eq(v1)
    f2.assert_eq(v2)
    if f3 is not None:
        f3.assert_eq(v3)

@cocotb.test()
async def reg_reg_operands(dut):
    s = SpadeExt(dut)

    # add ra, sp, gp
    await check_insn(s, "0x003100b3", s.o.rd, "1", s.o.rs1, "2", s.o.rs2, "3")

@cocotb.test()
async def i_imm(dut):
    s = SpadeExt(dut)

    # addi ra,sp,10
    await check_insn(s, "0x00a10093", s.o.rd, "1", s.o.rs1, "2", s.o.i_imm, "10")
    # addi ra,sp,10
    await check_insn(s, "0x7ff10093", s.o.rd, "1", s.o.rs1, "2", s.o.i_imm, "2047")
    # addi a0,a1,2047
    await check_insn(s, "0x7ff10093", s.o.rd, "1", s.o.rs1, "2", s.o.i_imm, "2047")
    # addi a0,a1,-10
    await check_insn(s, "0xff658513", s.o.rd, "10", s.o.rs1, "11", s.o.i_imm, "-10")
    # addi a0,a1,-2048
    await check_insn(s, "0x80058513", s.o.rd, "10", s.o.rs1, "11", s.o.i_imm, "-2048")

@cocotb.test()
async def u_imm(dut):
    s = SpadeExt(dut)

    # lui s4,0x1
    await check_insn(s, "0x00001a37", s.o.rd, "20", s.o.u_imm, "0b010000_0000_0000")
    # lui x20, 1048575
    await check_insn(s, "0xfffffa37", s.o.rd, "20", s.o.u_imm, "0xffff_f000")

@cocotb.test()
async def j_imm(dut):
    s = SpadeExt(dut)

    # jal x20, 2
    await check_insn(s, "0x00200a6f", s.o.rd, "20", s.o.j_imm, "2");
    # jal x20, -2
    await check_insn(s, "0xfffffa6f", s.o.rd, "20", s.o.j_imm, "-2");
    # Bit set in imm[11]
    # jal x20, 0b100000000000
    await check_insn(s, "0x00100a6f", s.o.rd, "20", s.o.j_imm, "0b100000000000");
    # All bits set in imm[10:1]
    # jal x20, 0x3fe
    await check_insn(s, "0x3fe00a6f", s.o.rd, "20", s.o.j_imm, "0x3fe")
    # All bits set in imm[19:12]
    # jal x20, 0x07c
    await check_insn(s, "0x07c00a6f", s.o.rd, "20", s.o.j_imm, "0x07c")
    # Bit set in imm[20]. Should sign extend
    # jal x20, -1048576
    await check_insn(s, "0x80000a6f", s.o.rd, "20", s.o.j_imm, "-1048576")
    # Max positive number
    # jal x20, 1048574
    await check_insn(s, "0x7ffffa6f", s.o.rd, "20", s.o.j_imm, "1048574")

@cocotb.test()
async def b_imm(dut):
    s = SpadeExt(dut)

    # beq x1,x2,2
    await check_insn(s, "0x00208163", s.o.rs1, "1", s.o.rs2, "2", s.o.b_imm, "2");
    # beq x1,x2,-2
    await check_insn(s, "0xfe208fe3", s.o.rs1, "1", s.o.rs2, "2", s.o.b_imm, "-2");
    # beq x1,x2,4094
    await check_insn(s, "0x7e208fe3", s.o.rs1, "1", s.o.rs2, "2", s.o.b_imm, "4094");
    # beq x1,x2,4094
    await check_insn(s, "0x80208063", s.o.rs1, "1", s.o.rs2, "2", s.o.b_imm, "-4096");
    # All ones in bit 1..4
    # beq x1,x2,0b11110
    await check_insn(s, "0x00208f63", s.o.rs1, "1", s.o.rs2, "2", s.o.b_imm, "0b11110");
    # All ones in bit 10..5
    # beq ...
    await check_insn(s, "0x7e208063", s.o.rs1, "1", s.o.rs2, "2", s.o.b_imm, "0b11111100000");
    # one in bit 11
    # beq ...
    await check_insn(s, "0x002080e3", s.o.rs1, "1", s.o.rs2, "2", s.o.b_imm, "0b100000000000");
    # one in bit 12 is tested by -4096

@cocotb.test()
async def s_imm(dut):
    s = SpadeExt(dut)
    # sw x1,1(x2)
    await check_insn(s, "0x001120a3", s.o.rs1, "2", s.o.rs2, "1", s.o.s_imm, "1")
    # sw x1,-1(x2)
    await check_insn(s, "0xfe20afa3", s.o.rs1, "1", s.o.rs2, "2", s.o.s_imm, "-1")
    # sw x1,2047(x2)
    await check_insn(s, "0x7e20afa3", s.o.rs1, "1", s.o.rs2, "2", s.o.s_imm, "2047")
    # sw x1,-2048(x2)
    await check_insn(s, "0x8020a023", s.o.rs1, "1", s.o.rs2, "2", s.o.s_imm, "-2048")
    # All ones in bit 0..4
    # sw x1,0b11111(x2)
    await check_insn(s, "0x0020afa3", s.o.rs1, "1", s.o.rs2, "2", s.o.s_imm, "0b11111")
    # All ones in bit 10..5
    # sw ..
    await check_insn(s, "0x7e20a023", s.o.rs1, "1", s.o.rs2, "2", s.o.s_imm, "0b011111100000")


