# top=memory::test_harness

from spade import *
from cocotb.clock import Clock

def ctrl(access_width, addr, command) -> str:
    return f"ControlSignals$(access_width: AccessWidth::{access_width}(), addr: {addr}, cmd: {command})"

def ctrl_write(access_width, addr, data) -> str:
    return ctrl(access_width, addr, f"Command::Write({data})")

def ctrl_read(access_width, addr) -> str:
    return ctrl(access_width, addr, "Command::Read()")

def ctrl_nop() -> str:
    return ctrl("Full", 0, "Command::Nop()")


def dual_bus(l: str, r: str) -> str:
    return f"({l}, {r})"

async def write_left(s, clk, access_width, addr, data):
    s.i.buses = dual_bus(ctrl_write(access_width, addr, data), ctrl_nop())
    await FallingEdge(clk)
    s.i.buses = dual_bus(ctrl_read(access_width, addr), ctrl_nop())

async def read_left(s, clk, access_width, addr, expected):
    s.i.buses = dual_bus(ctrl_read(access_width, addr), ctrl_nop())
    await FallingEdge(clk)
    s.o.l.assert_eq(expected)

async def read_both(s, clk, l, r):
    (laccess_width, laddr, lexpected) = l
    (raccess_width, raddr, rexpected) = r
    s.i.buses = dual_bus(
            ctrl_read(laccess_width, laddr),
            ctrl_read(raccess_width, raddr)
        )
    await FallingEdge(clk)
    s.o.l.assert_eq(lexpected)
    s.o.r.assert_eq(rexpected)

@cocotb.test()
async def main_tests(dut):
    s = SpadeExt(dut)

    s.i.range = "(0, 4096)"

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    await FallingEdge(clk)

    # Full width aligned writes and reads work
    await write_left(s, clk, "Full", 0, "0x10203040")
    await write_left(s, clk, "Full", 4, "11")

    await read_left(s, clk, "Full", 0, "0x10203040")
    await read_left(s, clk, "Full", 4, "11")

    # Signed byte reads work
    await read_left(s, clk, "ByteS", 0, "0x40")
    await read_left(s, clk, "ByteS", 1, "0x30")
    await read_left(s, clk, "ByteS", 2, "0x20")
    await read_left(s, clk, "ByteS", 3, "0x10")

    # Writing bytes does not mangle neighbouring bytes
    await write_left(s, clk, "ByteS", 0, '0x7f')
    await write_left(s, clk, "ByteS", 1, '0x6f')
    await read_left(s, clk, "ByteS", 0, "0x7f")
    await read_left(s, clk, "ByteS", 1, "0x6f")
    await read_left(s, clk, "ByteS", 2, "0x20")
    await read_left(s, clk, "ByteS", 3, "0x10")

    # Byte writes work and only overwrite the target byte
    await write_left(s, clk, "Full", 0, "0xffff_ffff")
    await write_left(s, clk, "ByteS", 3, "0x20")
    await write_left(s, clk, "ByteS", 2, "0x01")
    await read_left(s, clk, "Full", 0, "0x2001_ffff")

    # Half words work
    await write_left(s, clk, "HalfS", 0, "0x1234")
    await write_left(s, clk, "HalfS", 2, "0x5678")
    await write_left(s, clk, "HalfS", 5, "0x7bcd")
    await read_left(s, clk, "HalfS", 0, "0x1234")
    await read_left(s, clk, "HalfS", 2, "0x5678")
    await read_left(s, clk, "HalfS", 5, "0x7bcd")

    # Signed bytes and half words are sign extended
    await write_left(s, clk, "ByteS", 0, "0x80")
    await write_left(s, clk, "HalfS", 3, "0x8000")
    await read_left(s, clk, "ByteS", 0, "0xffff_ff80")
    await read_left(s, clk, "HalfS", 3, "0xffff_8000")

    # Unsigned bytes and half words are not sign extended
    await write_left(s, clk, "ByteU", 0, "0x80")
    await write_left(s, clk, "HalfU", 3, "0x8000")
    await read_left(s, clk, "ByteU", 0, "0x80")
    await read_left(s, clk, "HalfU", 3, "0x8000")

@cocotb.test()
async def unalined_full_read_offset1(dut):
    s = SpadeExt(dut)

    s.i.range = "(0, 4096)"

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    await FallingEdge(clk)

    await write_left(s, clk, "Full", 5, "0x1234_5678")
    await read_left(s, clk, "Full", 5, "0x1234_5678")

@cocotb.test()
async def unalined_full_read_offset2(dut):
    s = SpadeExt(dut)

    s.i.range = "(0, 4096)"

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    await FallingEdge(clk)

    await write_left(s, clk, "Full", 6, "0x1234_5678")
    await read_left(s, clk, "Full", 6, "0x1234_5678")

@cocotb.test()
async def unalined_full_read_offset3(dut):
    s = SpadeExt(dut)

    s.i.range = "(0, 4096)"

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    await FallingEdge(clk)

    await write_left(s, clk, "Full", 7, "0x1234_5678")
    await read_left(s, clk, "Full", 7, "0x1234_5678")

@cocotb.test()
async def unalined_full_read_offset3(dut):
    s = SpadeExt(dut)

    s.i.range = "(0, 4096)"

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    await FallingEdge(clk)

    await write_left(s, clk, "Full", 11, "20")
    await read_left(s, clk, "Full", 11, "20")

@cocotb.test()
async def both_read_ports_work(dut):
    s = SpadeExt(dut)

    s.i.range = "(0, 4096)"

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    await FallingEdge(clk)

    await write_left(s, clk, "Full", 11, "20")
    await write_left(s, clk, "Full", 30, "450")
    await read_both(s, clk, ("Full", 30, "450"), ("Full", 11, "20"))


@cocotb.test()
async def memory_offset_works(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i

    s.i.range = "(0x8000_0000, 0x8000_1000)"
    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    await FallingEdge(clk)

    await write_left(s, clk, "Full", 0x8000_0020, "11")
    await write_left(s, clk, "Full", 0x8000_0045, "30")
    await read_both(s, clk, ("Full", 0x8000_0045, "30"), ("Full", 0x8000_0020, "11"))


@cocotb.test()
async def out_of_range_writes_do_not_smash(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i

    s.i.range = "(0, 4096)"
    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    await FallingEdge(clk)

    await write_left(s, clk, "Full", 0, "11")
    await write_left(s, clk, "Full", 4096, "30")
    await read_left(s, clk, "Full", 0, "11")

