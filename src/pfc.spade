use lib::instructions;

enum PcState {
    // The program is stalled to wait for programming of the program memory
    WaitProgMem,
    // Program is running and is at the specified PC
    Run{pc: int<32>}
}

entity program_counter_internal(
    clk: clock,
    rst: bool,
    load_done: bool,
    stall: bool,
    jump: Option<int<32>>
) -> Option<int<32>> {
    reg(clk) state reset(rst: PcState::WaitProgMem()) = match state {
        PcState::WaitProgMem => if load_done {PcState::Run(~0x7fff_ffff)} else {PcState::WaitProgMem()},
        PcState::Run(pc) => match (jump, stall) {
            (Some(new_pc), _) => PcState::Run(new_pc),
            (None, true) => PcState::Run(trunc(pc)),
            // 4 because instructions are 4 bytes wide
            (None, false) => PcState::Run(trunc(pc + 4))
        }
    };

    match state {
        PcState::WaitProgMem => None(),
        PcState::Run(val) => Some(val)
    }
}

pipeline(1) program_counter(
    clk: clock,
    rst: bool,
    load_done: bool,
    stall: bool,
    jump: Option<int<32>>
) -> Option<int<32>> {
        let result = inst program_counter_internal$(clk, rst, load_done, stall, jump);
    reg;
        stage(-1).result
}



fn jump_target(
    op: instructions::Op,
    pc: int<32>,
    j_imm: int<32>,
    b_imm: int<32>,
    alu_value: int<32>,
    mepc: int<32>,
)
    -> int<32>
{
    let is_jalr = match op {
        instructions::Op::JALR => true,
        _ => false,
    };
    let is_mret = match op {
        instructions::Op::MRET => true,
        _ => false,
    };
    let is_ebreak = match op {
        instructions::Op::EBREAK => true,
        _ => false,
    };
    let rhs = match op {
        instructions::Op::JAL => j_imm,
        instructions::Op::JALR => 0,
        instructions::Op::BEQ => b_imm,
        instructions::Op::BNE => b_imm,
        instructions::Op::BLT => b_imm,
        instructions::Op::BGE => b_imm,
        instructions::Op::BLTU => b_imm,
        instructions::Op::BGEU => b_imm,
        instructions::Op::LUI => 0,
        instructions::Op::AUIPC => 0,
        instructions::Op::LB => 0,
        instructions::Op::LH => 0,
        instructions::Op::LW => 0,
        instructions::Op::LBU => 0,
        instructions::Op::LHU => 0,
        instructions::Op::SB => 0,
        instructions::Op::SH => 0,
        instructions::Op::SW => 0,
        instructions::Op::ADDI => 0,
        instructions::Op::SLTI => 0,
        instructions::Op::SLTIU => 0,
        instructions::Op::XORI => 0,
        instructions::Op::ORI => 0,
        instructions::Op::ANDI => 0,
        instructions::Op::SLLI => 0,
        instructions::Op::SRLI => 0,
        instructions::Op::SRAI => 0,
        instructions::Op::ADD => 0,
        instructions::Op::SUB => 0,
        instructions::Op::SLL => 0,
        instructions::Op::SLT => 0,
        instructions::Op::SLTU => 0,
        instructions::Op::XOR => 0,
        instructions::Op::SRL => 0,
        instructions::Op::SRA => 0,
        instructions::Op::OR => 0,
        instructions::Op::AND => 0,
        instructions::Op::NOP => 0,
        instructions::Op::CSRRW => 0,
        instructions::Op::CSRRS => 0,
        instructions::Op::CSRRC => 0,
        instructions::Op::CSRRWI => 0,
        instructions::Op::CSRRSI => 0,
        instructions::Op::CSRRCI => 0,
        instructions::Op::MRET => 0,
        instructions::Op::EBREAK => 0,
        instructions::Op::INVALID => 0,
    };

    if is_jalr {
        alu_value
    }
    else if is_mret {
        mepc
    }
    else if is_ebreak {
        // Kind of cursed, but if we just jump to the PC we can simulate stopping
        // here. We could also do pc+4 if an external signal is set, in case
        // we want to be able to unbreak
        pc
    }
    else {
        trunc(pc + rhs)
    }
}

